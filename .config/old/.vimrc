filetype plugin indent on
let mapleader = "\\"

nnoremap <leader>r :make run<CR>
nnoremap <Leader>cp :AsyncRun latexmk -pvc -pdf %<CR>
nnoremap <Leader>e :Rexplore <CR>
nnoremap <leader>q :copen<CR>
nnoremap <leader>c :cclose<CR>
"nnoremap <leader>t :term make run<CR>
nnoremap <leader>l :silent make<CR>:redraw!<CR>
autocmd TerminalOpen * nnoremap <buffer> f <C-W>q
nnoremap <leader>t :tabnew<CR>:term ++curwin make run<CR>
nnoremap <leader>l :tabnew<CR>:term ++curwin make<CR>

nnoremap <leader>j :b 

function Getdir()
	call inputsave()
	let g:dir = input("dir: ")
	call inputrestore()
endfunction

nnoremap <leader>f :call Getdir()<CR>:find <C-r>=dir<CR><CR>

let g:netrw_keepdir=1
let c_no_curly_error=1
let loaded_matchparen = 1
let loaded_matchparen = 1

hi Number    ctermfg=NONE
highlight! LineNr ctermfg=NONE
nnoremap <leader>l :tabnew<CR>:term ++curwin make<CR>
"set wildmode=list,full
"set wildmenu

syntax on
set number
set relativenumber
inoremap ` <esc>
inoremap <C-@> <C-m>
inoremap <C-k> <C-h>
"let g:netrw_banner=0        " disable banner
"nnoremap <Leader>fe :AsyncRun markdown.sh %<CR>
"nnoremap <Leader>cl :! cargo run <CR>
set path=.,*
"set autochdir
