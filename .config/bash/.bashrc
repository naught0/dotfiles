
# ~/.bashrc
#
#osc7_cwd() {
#    local strlen=${#PWD}
#    local encoded=""
#    local pos c o
#    for (( pos=0; pos<strlen; pos++ )); do
#        c=${PWD:$pos:1}
#        case "$c" in
#            [-/:_.!\'\(\)~[:alnum:]] ) o="${c}" ;;
#            * ) printf -v o '%%%02X' "'${c}" ;;
#        esac
#        encoded+="${o}"
#    done
#    printf '\e]7;file://%s%s\e\\' "${HOSTNAME}" "${encoded}"
#}
#PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }osc7_cwd


case ${TERM} in
  foot*)
	PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s\007" "${PWD/#$HOME/\~}"'
    ;;
esac


# If not running interactively, don't do anything
[[ $- != *i* ]] && return

source ~/.secret.env
source /etc/profile.d/vte.sh
alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '
PS1="[\u] \w> "
#PS1="> "
#PS1='\u@\h:\W\$ '
alias new='launch alacritty --working-directory=.'
#alias vim='/usr/bin/nvim'
alias v='/usr/bin/vim'
export MANWIDTH=80
export EDITOR=/usr/bin/vim

set -o vi

bind '"\C-a":"cd $(fd . | sk)\n"'
#alias info='info --vi-keys'
alias mutt='neomutt'
source /etc/profile.d/vte.sh
#PROMPT_COMMAND=ls
#cd $(cat ~/.local/state/whereami)
PATH="$HOME/.local/bin:$PATH"

function cd {
    builtin cd "$@" && ls
}
