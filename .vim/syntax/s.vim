" Vim syntax file
" Language:		GNU Assembler
" Maintainer:		Doug Kearns dougkearns@gmail.com
" Previous Maintainers: Erik Wognsen <erik.wognsen@gmail.com>
"			Kevin Dahlhausen <kdahlhaus@yahoo.com>
" Contributors:		Ori Avtalion, Lakshay Garg
" Last Change:		2020 Oct 31

" quit when a syntax file was already loaded
