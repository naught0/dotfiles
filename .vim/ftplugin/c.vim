"execute ':map <buffer> <Leader>r :tabnew<CR>:term ++curwin ++shell cc ' . expand("%") . ' && ./a.out<CR>'

"execute ':map <buffer> <Leader>r :tabnew<CR>:term ++curwin bash --rcfile .comrc<CR> cc '. expand("%") . '<CR> ./a.out<CR>'
execute ':map <buffer> <Leader>r :tabnew<CR>:term ++curwin make run -f ~/pub/Makefile arg="'. expand("%") . '"<CR>'
"execute ':map <buffer> <Leader>r :tabnew<CR>:term ++curwin tcc -run -b -Wall -Werror "'. expand("%") . '"<CR>'

"nnoremap <leader>a :!fd \| grep -e .c -e .h \| xargs ctags<CR>
"
nnoremap <leader>a :!find -name '*.c' -o -name '*.h' \| xargs ctags<CR>
"execute ':map <buffer> <Leader>b :tabnew<CR>:term ++curwin ++shell  cc '. expand("%") . ' && ./a.out<CR><C-W>N'

"hi For
"hi Normal    ctermbg=230
"hi Statement term=NONE ctermfg=NONE cterm=bold
"hi Type      term=NONE ctermfg=NONE
""hi String    ctermfg=NONE
"""hi cFormat    ctermfg=black cterm
hi Special    ctermfg=1
hi Number    ctermfg=NONE
"hi Comment   ctermfg=darkgrey
hi PreProc   ctermfg=NONE
hi Constant  ctermfg=NONE
hi String  ctermfg=4
hi Include  ctermfg=NONE
hi cInclude  ctermfg=NONE
highlight! LineNr ctermfg=NONE

